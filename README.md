### Things to prepare before arrival


#### Nursery items

| Item  | Min Qty | Buy from? | Info |
| ------------- | ------------- | ------------- | ------------- |
| Bassinet  | 1 |   |   |
| Bassinet sheets  | 2 x fitted & flat |   |   |
| Muslin wraps  | 6  | Target/baby bunting | Get the bigger ones because they help with swaddling (cotton/bamboo) weego amigo|
| Full body suits  | 6-8  |bonds/kmart/target   | Remember to wash everything prior to use with a little bit of dettol and baby soap |
| Singlets  | 2 packs of 0000, 2 packs 000 |   | Remember to wash everything prior to use with a little bit of dettol and baby soap |
| Socks  | 5 pairs |   | Remember to wash everything prior to use with a little bit of dettol and baby soap |
| Beanie  | 1 |   | Remember to wash everything prior to use with a little bit of dettol and baby soap |
| Sunhat  | 1 |   | Remember to wash everything prior to use with a little bit of dettol and baby soap |
| Mittens  | 1 |   | Remember to wash everything prior to use with a little bit of dettol and baby soap |
| Baby Monitor  | 1 |   | One which connects to WiFi |
| Baby head support | 1 |   | baby bunting |
| Cot mobile | 1 |   | baby bunting |


#### Changing Items

| Item  | Min Qty | Buy from? | Info |
| ------------- | ------------- | ------------- | ------------- |
| Hand disinfectant  | 4 (every room) |   | Keep everywhere |
| Wet Wipes  | Stock it |   | Huggies wipes |
| Diapers  | Stock it |   | Huggies first/ Baby love (coles)|
| Nappy Sacks  | Stock it |  Coles/Wollies |  |
| Diaper ointment cream  | 1 or 2 | Chemist warehouse  | Sudocream/bepanthen |
| Change mat  | 1 | Baby bunting  |  |
| Change mat Cover | 1 | Baby bunting  | Get some good designs |
| Change Table  | 1 |   | Standard size |

#### Bathing Times

| Item  | Min Qty | Buy from? | Info |
| ------------- | ------------- | ------------- | ------------- |
| Bath tub  | 1 |   |  |
| Bath Thermometer  | 1 |   | Good to maintain a constant temp |
| Bath towels  | 2 |   | Get the soft towels |
| Face washers  | 10pk |   |  |
| Nappy changes for sore bottoms  | 10pk |   |  |
| Burping spills  | 10pk |   |  |
| Body wash  |  |  Chemist warehouse | QV/Gaya |
| Bath oil  |  | Chemist warehouse  | QV/Gaya |
| Massage oil | 1 |   | chemist warehouse |
| Moisturiser |  | Chemist warehouse | QV/Gaya |
| Baby nail clippers | Baby bunting | Coles |  |
| Baby buds |  |  | Big ones |


#### Outdoor

| Item  | Min Qty | Buy from? | Info |
| ------------- | ------------- | ------------- | ------------- |
| Pram | 1 |   | Baby bunting  |
| Pram clips | 4 |   | eBay/ catch of the day/bunting |
| Car seat | 1 |   | Baby bunting |
| Sun shade/ | 1 |   | baby bunting |
| Baby carrier  | 1 |   |  |
| Pram liner (cotton) | 1 |   | Baby bunting  |
| Pram toys | sound/ colour stimulation |   | baby bunting |


#### Feeding

| Item  | Min Qty | Buy from? | Info |
| ------------- | ------------- | ------------- | ------------- |
| Breast pump | 1 |   | Madela double electric pump  |
| Bibs | 10 |   |  |
| Formula for feeding |  |   | Consult hospital Nan Pro gold |
| Bottles | 2-3 |   | Get Avent starter kit/Pigeon wide neck bottles |
| tommee tippe pacifiers dummies | 1 |   | chemist warehouse |
| Bottle warmer | 1 |   |  |
| Bottle steriliser | 1 |   | Important |
| Bottle brush | 1 |   |  |
| Rockin chair | 1 |   | For peace of mind :) |
| Thermos for hot water storage | 1 |   |  :) |

#### First Aid

| Item  | Min Qty | Buy from? | Info |
| ------------- | ------------- | ------------- | ------------- |
| Thermometer | 1 |   |   |
|  Fess Little Noses Saline Drops | 1 |   | Nose blocking :(  |
| Infocol/ infants friend | 1 |   | chemist warehouse :) |

#### Hospital Kit (kid)

| Item  | Min Qty | Buy from? | Info |
| ------------- | ------------- | ------------- | ------------- |
| Diapers | 1 |   | Softpack  |
| Wipes | 2-3 |   |   |
| Cotton woll balls |  |   |   |
| Cotton buds
| Face washers |  |   |   |
| Body suits/ Onesies |  2 |   |   |
| Singlets |  4-6 |   |  2 per day |
| Muslin wraps |  4 |   |   |
| Bunny rugs | 2 |   |   |
| Going home outfit | 1 |   |   |
| Plastic bag for dirty cloths | 1 |   |   |

#### Hospital Kit (mother)

| Item  | Min Qty | Buy from? | Info |
| ------------- | ------------- | ------------- | ------------- |
| Coconut water | 2 |   |   |
| Nut bars/ energy bars| 10 |   |   |
| gym ball | 1 |   |   |
| Breast pads | 1 box |   |   |
| Maternity pads | 2 packets |   |   |
| Underwear | 12 |  Size  - L |   preferably black and avoid low-rise if having a caesarean |
| Maternity bras | 3 |  Bonds/ Target |  |
| Pyjamas/nighties |  2 | KMart/Target  |   |
| Slippers |  4 |   |   |
| Nursing tops | 2 |   |   |
| Toileteries (shampoo, bath, conditioner) | 1 |   |   |
| Camera | 1 |   | To get the best first click  |
| Phone & charger | 1 |   | |
| Going home outfit | 1 |   |   |
| Make up items for mummy |  |   |   |
